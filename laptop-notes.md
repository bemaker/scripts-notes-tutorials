# fablabmobile / beMaker laptop notes

Since April 2018 all the beMaker laptops have been switched to Ubuntu 14.04

Laptops are sequenced 1 to 12, their numbers are both written on the yellow circular sticker on the back of the screens, and also on login, the laptop user names are equal to the laptop number.

bemaker03 = laptop number 3

1 → 12 Thinkpad Helix convertible laptops, note that they are not meant to be separated from their keyboard, ubuntu unity windoz manager does not play super well with touch screens, therefore they have two charging ports, one on the base, on the backside of the laptop, the second is on the tablet portion, bottom left side.

Due to their original tablet nature, there is a lock screen button on the top of the tablet, it sends the laptot to the suspend state. Nothing in Ubuntu lets you know that the machine is suspended and not off, apart from the LED in the ThinkPad logo on the back of the tablet. If in doubt, long press the power button (15 seconds or more) until the lead stops fading in and out, then reboot.

Meanwhile power management is vital, leaving the laptops on standby overnight or multiple days in their boxes will cause trouble to the next team using them with students. Get the students to help you power off the laptops at the end of each class.

do not worry about the desktop saying there is a
System program problem detected, this is the Ubuntu Unity system catching errors and wanting a report.
